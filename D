mixed-port: 7890
redir-port: 7891
allow-lan: true
mode: rule
log-level: silent
external-controller: :9090

proxy-groups:
  - name: ￥
    type: select
    proxies:
      - AUTO
    use:
      - a
  - name: AUTO
    type: url-test
    use:
      - a

proxy-providers:
  a:
    type: http
    url: https://raw.githubusercontents.com/oslook/clash-freenode/main/clash.yaml
    path: d.yaml
    health-check:
      enable: true
      url: http://www.gstatic.com/generate_204
      interval: 86400
      tolerance: 50

rules:
  - DOMAIN-SUFFIX,ad.com,REJECT
  - GEOIP,CN,DIRECT
  - MATCH,￥